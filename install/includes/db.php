<?php 

$sql = "
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner` text NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `pg` int(11) NOT NULL ,
  `link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `banners`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `diretorio`
--

CREATE TABLE IF NOT EXISTS `diretorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `diretorio`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE IF NOT EXISTS `endereco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cep` varchar(100) NOT NULL,
  `endereco` text NOT NULL,
  `numero` int(11) NOT NULL,
  `bairro` text NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `uf` varchar(100) NOT NULL,
  `telefone` varchar(100) NOT NULL,
  `email` text NOT NULL COMMENT 'formulario de contato',
  `pg` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `endereco`
--
-- --------------------------------------------------------


--
-- Estrutura da tabela `filessite`
--

CREATE TABLE IF NOT EXISTS `filessite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` text NOT NULL,
  `author` int(11) NOT NULL,
  `date` date NOT NULL,
  `diretorio` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `filessite`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE IF NOT EXISTS `fotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comentario` text NOT NULL,
  `img` text NOT NULL,
  `galeria` int(11) NOT NULL,
  `tipo` int(11) NOT NULL COMMENT '1= foto, 2 = video',
    `img2` text NOT NULL,
     `titulo` varchar(100) NOT NULL,
	  `link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `fotos`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `data` varchar(100) NOT NULL,
  `capa` text NOT NULL,
  `pg` int(11) NOT NULL,
  `coment` text NOT NULL,
   `destaque` int(11) NOT NULL,
    `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `galeria`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `logs`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `cadastro` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `news`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `nivel`
--

CREATE TABLE IF NOT EXISTS `nivel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `pg` text NOT NULL,
  `editar` int(11) NOT NULL,
  `excluir` int(11) NOT NULL,
  `visualizar` int(11) NOT NULL,
  `cadastrar` int(11) NOT NULL,
  `detalhes` int(11) NOT NULL,
  `chat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `nivel`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE IF NOT EXISTS `paginas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `tipo` int(11) NOT NULL COMMENT '1 = normal, 2 = noticias, 3 = galeria',
  `posicao` int(11) NOT NULL,
  `sub` int(11) NOT NULL,
  `img` int(11) NOT NULL,
  `width_cate` int(11) NOT NULL,
  `height_cate` int(11) NOT NULL,
  `width_img` int(11) NOT NULL,
  `height_img` int(11) NOT NULL,
  `width_mini` int(11) NOT NULL,
  `height_mini` int(11) NOT NULL,
  `html1` text NOT NULL,
  `html2` text NOT NULL,
  `html3` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `paginas`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `textos`
--

CREATE TABLE IF NOT EXISTS `textos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `img1` text NOT NULL,
  `img2` text NOT NULL,
  `img3` text NOT NULL,
  `img4` text NOT NULL,
  `video` text NOT NULL,
  `link` text NOT NULL,
  `tipo` int(11) NOT NULL,
  `pg` int(11) NOT NULL,
  `posicao` int(11) NOT NULL,
  `destaque` int(11) NOT NULL,
  `subtitulo` varchar(100) NOT NULL,
   `cate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `textos`
--

--
-- Estrutura da tabela `select`
--

CREATE TABLE IF NOT EXISTS `select` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campo1` varchar(100) NOT NULL,
  `campo2` varchar(100) NOT NULL,
  `campo3` varchar(100) NOT NULL,
  `campo4` varchar(100) NOT NULL,
  `tipo` int(11) NOT NULL,
  `pg` int(11) NOT NULL,
  `posicao` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Estrutura da tabela `avisos`
--

CREATE TABLE IF NOT EXISTS `avisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aviso` varchar(100) NOT NULL,
  `msg` text NOT NULL,
  `usuario` int(11) NOT NULL,
  `inicio` varchar(100) NOT NULL,
  `fim` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `posicao` int(11) NOT NULL,
  `data` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `textos`
--

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(100) NOT NULL,
  `datafin` varchar(100) NOT NULL,
  `esta` varchar(100) NOT NULL,
  `envio` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` text NOT NULL,  
  `nome2` varchar(100) NOT NULL,
  `email2` text NOT NULL,
  `valorcv` text NOT NULL,
  `valorav` text NOT NULL,
  `valorrp` text NOT NULL,
  `valorsub` text NOT NULL,
  `valorfgts` text NOT NULL,
  `valorfin` text NOT NULL,
  `agencia` varchar(100) NOT NULL,
  `origem` int(11) NOT NULL,
  `nota` text NOT NULL,
  `imovel` int(11) NOT NULL,
  `modalidade` int(11) NOT NULL,
  `anexo` text NOT NULL,
   `telefone` varchar(100) NOT NULL,
  `cpf` varchar(100) NOT NULL,
  `rg` varchar(100) NOT NULL,
  `nascimento` varchar(100) NOT NULL,
  `civil` int(11) NOT NULL,
  `telefone2` varchar(100) NOT NULL,
  `cpf2` varchar(100) NOT NULL,
  `rg2` varchar(100) NOT NULL,
  `nascimento2` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `civil2` int(11) NOT NULL,
  `pg` int(11) NOT NULL,
  `posicao` int(11) NOT NULL,
  `finalizado` varchar(100) NOT NULL,
   `prioridade` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `clientes`
--


-- --------------------------------------------------------



--
-- Estrutura da tabela `processos`
--

CREATE TABLE IF NOT EXISTS `processos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etapa` varchar(100) NOT NULL,
  `nota` text NOT NULL,
  `data` varchar(100) NOT NULL,
  `cliente` int(11) NOT NULL,
  `status` int(11) NOT NULL, 
  `usuario` int(11) NOT NULL, 
  `usuariocc` int(11) NOT NULL, 
  `agencia` int(11) NOT NULL, 
  `pg` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  `comprador` int(11) NOT NULL,
  `finalizado` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `processos`
--

CREATE TABLE IF NOT EXISTS `data_envio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `processos`
--



-- --------------------------------------------------------


--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_mail` varchar(100) NOT NULL,
  `user_senha` varchar(100) NOT NULL,
  `user_date` date NOT NULL,
  `user_nivel` int(11) NOT NULL COMMENT '0 para master , 2 para adm e 3 para user',
  `user_status` int(1) NOT NULL COMMENT '1 para ON e 2 para OFF',
  `chave` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `from` TEXT NOT NULL,
  `to` TEXT NOT NULL,
  `message` TEXT NOT NULL,
  `sent` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

";


if($_REQUEST['serv_db'] != ""){

		$end_site = $_REQUEST['end_site'];
		$directorio = $_REQUEST['directorio'];
		$servidor = $_REQUEST['serv_db'];
		$usuario_banco = $_REQUEST['usuario_db'];
		$senha_banco = $_REQUEST['senha_db'];
		$verificar_conexao = mysql_connect($servidor,$usuario_banco,$senha_banco) or die(mysql_error());
	if($verificar_conexao == true){
	
		$banco_de_dados = $_REQUEST['banco_db'];
		$verficiar_banco = mysql_select_db($banco_de_dados,$verificar_conexao) or die(mysql_error());
	if($verficiar_banco == true){
	if($_REQUEST['email_suporte'] != "" AND $_REQUEST['senha_suporte'] != ""){
	$criar_tabelas = explode("CREATE TABLE",$sql);
	for($i=0;$i<count($criar_tabelas);$i++){
	$cadastra_tudo = mysql_query("CREATE TABLE ".$criar_tabelas[$i]);
}
	if($cadastra_tudo == true){
		$p1 = explode("@",$_REQUEST['email_suporte']);
					$p2 = date("Y-m-d");
					$chave = md5($p1[0].$p2[1]);
	$senha = md5($_REQUEST['senha_suporte']);
	$gravar_usuario = mysql_query("INSERT INTO usuarios (user_name,user_mail,user_senha,user_date,user_nivel,user_status,chave) VALUES ('Suporte','$_REQUEST[email_suporte]','$senha','$p2','0','0','$chave')");
	if($gravar_usuario == true){
	 // config admin-config
	 $caminho = '../../adm/admin-config.php';
	$criar = fopen($caminho, 'w');
	
	
$endereco_http_explode = explode("http://",$end_site);
	if($endereco_http_explode[1] != ""){
		$www_explo = explode("www.",$endereco_http_explode[1]);
		if($www_explo[1] != ""){
		$endereco_http = "http://".$www_explo[1];
		$endereco_http_www = $end_site;
	}else{
		$endereco_http = $end_site;
		
	$endereco_http_www = "http://www.".$endereco_http_explode[1];
	}
}else{

	$www_explo = explode("www.",$end_site);
	if($www_explo[1] != ""){
	$endereco_http = "http://".$www_explo[1];
	$endereco_http_www = "http://www.".$www_explo[1];
	}else{
	$endereco_http = "http://".$end_site;
	$endereco_http_www = "http://www.".$end_site;
	}
}



	$conf ='
	<?php
	//Url _ não é necessário
	@define("Config_Host", "'.$endereco_http.'");
	
	//Nome do cliente _ não é necessário
	@define("Config_Cliente_Nome", "'.$endereco_http_www.'/'.$directorio.'");
	
	//Ip para acesso ao banco de dados
	@define("Config_ip", "'.$servidor.'");
	
	//Banco de dados ultilizado	
	//Banco de dados ultilizado	
	@define("Config_Banco", "'.$banco_de_dados.'");
	
	//Usuário multi privilegios do banco (Leitura, Escrita, Exclusão e Edição)
	@define("Config_Master_Banco", "'.$usuario_banco.'");
	
	//Senha Multi-Privilégios
	@define("Config_Master_Pass", "'.$senha_banco.'");

	//Usuario não preferêncial (somente leitura)
	@define("Config_User_Banco", "");	
	
	//Senha de leitura
	@define("Config_User_Pass", "");
	
	//Níveis de usuários
	@define("Web_Grupos", "2");
	
	//Caminho para login usuário administrativo
	@define("Web_Url_Admin", "m/index.php");
	
	//Caminho para login usuário visitante		
	@define("Web_Url_Padrao", Config_Cliente_Nome."/index.php");
	
	//Url de Saída	
	@define("Config_Sair", Config_Cliente_Nome."/index.php");
	
	//email de contato do cliente	
	@define("Config_Contact_Client", "");
	
	//Caminho onde as imagems deverão ser salvas
	@define("CaminhoImagensUpload", "/'.$directorio.'/Uploads");	
	
	//Caminho onde as imagems deverão ser salvas
	@define("Criatedby", "Nl2br");
	@define("Criatedbyrodape", "Claudio PHP");
	?>
	';
	
$criar_adm = fwrite($criar, $conf);
// ----- CRIANDO CONEXÃO ----- //
if($criar_adm){
 $caminho = '../../../conexao_db.php';
	$criar_conn_des = fopen($caminho, 'w');
$conteu_con ='
	<?php 
		$liberarCon = 2;
		require("'.$directorio.'/adm/conn2.php");
		include "'.$directorio.'/site/functions_site.php";
	?>
';
$criar_con_web = fwrite($criar_conn_des, $conteu_con);

	if($criar_con_web){
	echo 10;
	}
	}
	}
	}else{
	echo 20;
	}
	}else{
	echo 10;
	}
	}else{
	echo 20;
	}
	}else{
	echo 30;
	}
}else{
	echo 30;
}

?>