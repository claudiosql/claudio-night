	<?php 
	
	$select1 = "step-no"; 
	$select1_fundo = "step-dark-left"; 
	$select1_risco = "step-dark-right"; 
	
	$select2 = "step-no-off"; 
	$select2_fundo = "step-light-left"; 
	$select2_risco = "step-light-right"; 
	
	$select3 = "step-no-off"; 
	$select3_fundo = "step-light-left"; 
	$select3_risco = "step-light-round"; 
	

include "../includes/configurar.php"; ?>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">

<tr>
	<th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	
	
<div id="page-heading"><h1>Configurações do Administrador</h1></div>
	
		<!-- start id-form -->
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		
		<tr>
			<th valign="top">Endereço do site: </th>
			<td style="width:430px;"><input type="text" id="end_site" class="inp-form-error" />
			<div class="error-left"></div>
			<div class="error-inner">Exemplo: <strong>http://seusite.com.br</strong></div>
			</td>
		</tr>
		<tr>
			<th valign="top">Nome do diretório ADM:</th>
			<td style="width:430px;"><input type="text" id="directorio" class="inp-form-error" />
			<div class="error-left"></div>
			<div class="error-inner">Exemplo: <strong>empresa-admin</strong></div>
			</td>
		</tr>
		<tr>
			<th valign="top">Servidor Mysql:</th>
			<td style="width:430px;"><input type="text" id="servidor_db" class="inp-form-error" />
			<div class="error-left"></div>
			<div class="error-inner">Exemplo: <strong>localhost</strong></div>
			</td>
		</tr>
		<tr>
			<th valign="top">Nome B. Dados:</th>
			<td style="width:430px;"><input type="text" id="nome_db" class="inp-form-error" />
			</td>
		</tr>
		
		<tr>
			<th valign="top">Usuário do B. Dados:</th>
			<td style="width:430px;"><input type="text" id="usuario_db" class="inp-form-error" />
			
			</td>
		</tr>	<tr>
			<th valign="top">Senha do B. Dados:</th>
			<td style="width:430px;"><input type="text" id="senha_db" class="inp-form-error" />
			
			</td>
		</tr><tr>
		<td></td>
			<th valign="top"><a href="javascript:Verificar_con();"><input id="btTestDB" class="button" type="submit" value="Verificar Conexão"></a></th>
		
		</tr>
	<tr>
		<td></td>
		
		</tr><tr>
		<td></td>
		
		</tr>
			
	<tr>
			<th valign="top">E-mail Administrativo:</th>
			<td style="width:430px;"><input type="text" id="email_suporte" class="inp-form-error" />
			<div class="error-left"></div>
			<div class="error-inner">Exemplo: <strong>suporte@site.com.br</strong></div>
			</td>
		</tr><tr>
			<th valign="top">Senha Administrativa:</th>
			<td style="width:430px;"><input type="password" id="senha_suporte" class="inp-form-error" />
		
			</td>
		</tr>
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
		
			<a href="javascript:Pg_passo('pages/paginas_db.php')"><input id="btTestDB" class="button" type="submit" value="Avançar"></a>
		</td>
		<td></td>
	</tr>
	</table>
	<!-- end id-form  -->

	</td>
	<td>

	<!--  start related-activities -->
	<div id="related-activities">
		
		<!--  start related-act-top -->
		<div id="related-act-top">
		<img src="images/forms/header_related_act.gif" width="271" height="43" alt="" />
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">
			
				<div class="left"><h3 style="position: relative; top: -40px; width: 100px;">Guia Rápido</h3></div>
				<div class="right">
					<h5>Criando Banco de dados</h5>
					Para utilizar essa ferramente em seu web-site sera necessário:
					<br />
										<ul class="greyarrow">
					<li><a><strong>Provedor de Hospedagem</strong></a></li>
					<li><a><strong>Hospedagem Linux</strong></a></li>
					<li><a><strong>Banco de dados Mysql</strong></a></li>
					</ul>

				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/forms/icon_minus.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Install</h5>
					Exclua a pasta Install após concluir para sua segurança
				
				</div>
				
				<div class="clear"></div>
				<div class="lines-dotted-short"></div>
				
				<div class="left"><a href=""><img src="images/forms/icon_edit.gif" width="21" height="21" alt="" /></a></div>
				<div class="right">
					<h5>Informações</h5>
					Instalando o administrador
					<ul class="greyarrow">
						<li><a href="#">Endereço do site</a><p>
						Este campo é necessário para as configurações de Uploads de imagens futuros em seu Web-Site.
						</p></li> 
						<li><a href="#">Servidor Mysql:</a><p>
						Os dados de servidores são fornecidos na criação do banco de dados no provedor contratado.
						</p></li> 
						<li><a href="#">E-mail Administrativo:</a><p>
						Este E-mail será de suporte ao cliente e administração de páginas adminsitrativas.
						</p></li> 
					</ul>
				</div>
				<div class="clear"></div>
				
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->

</td>
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
 
<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>

<div class="clear">&nbsp;</div>
