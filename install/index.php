<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CMS Install</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen" title="default" />
<!--[if IE]>
<link rel="stylesheet" media="all" type="text/css" href="css/pro_dropline_ie.css" />
<![endif]-->

<!--  jquery core -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
 
<!--  checkbox styling script -->
<script src="js/jquery/ui.core.js" type="text/javascript"></script>
<script src="js/jquery/ui.checkbox.js" type="text/javascript"></script>
<script src="js/jquery/jquery.bind.js" type="text/javascript"></script>


<!--  styled select box script version 3 --> 


<script src="js/jquery/custom_jquery.js" type="text/javascript"></script>



<script type="text/javascript">
Pg_passo("pages/db_config.php");
function Pg_passo(pg){
$(function(){
		$("#carregando").fadeIn(600,function(){
if(pg == "pages/paginas_db.php"){
	if($("#servidor_db").val() == ""){
		alert('Preencha os Campos Obrigatórios');
		$("#carregando").fadeOut(600);
	}else if($("#nome_db").val() == ""){        
		alert('Preencha os Campos Obrigatórios');
		$("#carregando").fadeOut(600);
	}else if($("#usuario_db").val() == ""){     
		alert('Preencha os Campos Obrigatórios');
		$("#carregando").fadeOut(600);
	}else if($("#logo_db").val() == ""){        
		alert('Preencha os Campos Obrigatórios');
		$("#carregando").fadeOut(600);
	}else if($("#email_suporte").val() == ""){  
		alert('Preencha os Campos Obrigatórios');
		$("#carregando").fadeOut(600);
	}else if($("#senha_suporte").val() == ""){  
		alert('Preencha os Campos Obrigatórios');
		$("#carregando").fadeOut(600);
	}else{
$.post("includes/db.php",{
	end_site:$("#end_site").val(),
	directorio:$("#directorio").val(),
	serv_db:$("#servidor_db").val(),
	banco_db:$("#nome_db").val(),
	usuario_db:$("#usuario_db").val(),
	senha_db:$("#senha_db").val(),
	email_suporte:$("#email_suporte").val(),
	senha_suporte:$("#senha_suporte").val()
	}, function(responder){
	if(responder == 10){
		alert('Administrador Criado Com sucesso!');
		var end_site = $("#end_site").val();
		var direct_site = $("#directorio").val();
		location.href= end_site+'/'+direct_site;

	}else{
	$(".msg_red").html("Não foi Possivel cadastrar o Banco de dados!");
		$("#carregando").fadeOut(600);
		$("#message-red").show();
	}
	
	});
}}else{
	$("#content").load(pg,function(){
		$("#carregando").fadeOut(600);
		
	});

}
});

	});
}
function Verificar_con(){
$(function(){
$("#message-red").hide();
$("#message-green").hide();
$("#carregando").fadeIn(600);

	$.post("includes/db.php",{
	serv_db:$("#servidor_db").val(),
	banco_db:$("#nome_db").val(),
	usuario_db:$("#usuario_db").val(),
	senha_db:$("#senha_db").val()
	}, function(responde){
	if(responde == 10){
	$(".msg_gren").html("Conexão ao Servidor e Banco de dados válidos!");
	$("#message-green").show();
	$("#carregando").fadeOut(400);
	}else{
	if(responde == 20){
	$(".msg_red").html("Não foi enconstrado nenhum Banco da dados!");
	}else{
	$(".msg_red").html("Não foi Possivel conectar ao servidor!");
	}
	$("#message-red").show();
	$("#carregando").fadeOut(400);
	}
	});
	
});
}


</script>
</head>
<body> 
<!-- Start: page-top-outer -->
<?php include "includes/topo.php"; ?>
<!-- start content -->
<div id="content">
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php include "includes/rodape.php"; ?>
</body>
</html>