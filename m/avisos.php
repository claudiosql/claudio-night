﻿<?php 

//Verificando se o usuário está logado
	require("../reconhece.php");


//funções da classe usuário
	require("avisos/functions.php");
	//Meus resultados
	$texto = Listar_texto($_GET['user']);
	$carrega_texto = Mostra_texto($_GET['id']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo Criatedby; ?> Administrator</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />

<link rel="stylesheet" href="http://jqueryui.com/themes/base/jquery.ui.all.css"> 
<script type="text/javascript" src="../js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="../js/legendas.js"></script>

<script type="text/javascript" src="../js/jquery.maskedinput-1.2.2.js"></script>
<!-- Funções -->
<script type="text/javascript" src="avisos/functions.js"></script>
<!-- Fim Uusarios Funções -->
<link rel="stylesheet" type="text/css" href="../js/modal/shadowbox.css" />
<script src="../js/modal/shadowbox.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/tiny_mce/tiny_mce.js"></script>
<link type="text/css" href="../css/start/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="../js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript" src="../js/jquery-ui.min.js"></script>
<script type="text/javascript">
Shadowbox.init({
    language: 'pt-BR',
    players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
	
});
tinyMCE.init({

		language : "pt",
		// General options
		mode : "none",
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

		// Theme options
		theme_advanced_buttons1 : "pastetext,pasteword,bold,italic,underline,strikethrough,|,print,image,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,link,charmap,forecolor,media,preview,fullscreen,code",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : false,
		theme_advanced_resizing : false,
		   paste_auto_cleanup_on_paste : true,
        paste_preprocess : function(pl, o) {
            // Content string containing the HTML from the clipboard
            alert(o.content);
            o.content = "-: CLEANED :-\n" + o.content;
        },
        paste_postprocess : function(pl, o) {
            // Content DOM node containing the DOM structure of the clipboard
            alert(o.node.innerHTML);
            o.node.innerHTML = o.node.innerHTML + "\n-: CLEANED :-";
        },


	// Theme options
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_resizing : false,

		// Example content CSS (should be your site CSS)
	 content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",
		file_browser_callback : "tinyBrowser",
		
	
	});
$(function(){
$('#inicio').mask('99/99/9999');
$('#fim').mask('99/99/9999');
$('#bloco_5').hide();
	$('.sumir_bloco').hide();
<?php if($_GET['id'] != ""){?>
	$('#bloco_5').show();
	$('.sumir_bloco').show();
	$('.titulo_bloco_6').removeClass('selected_bloco');
	$('#cadastrar_cli').find('.titulo_bloco_6').addClass('selected_bloco');
	$('.cadastro_some').hide();
	<?php } ?>
	$('#cadastrar_cli').click(function(){
	$('.titulo_bloco_6').removeClass('selected_bloco');
	$('#bloco_5').show();
	$('.sumir_bloco').show();
	$(this).find('.titulo_bloco_6').addClass('selected_bloco');
	$('.cadastro_some').hide();
	});
	$('#visualizar_cli').click(function(){
	$('.titulo_bloco_6').removeClass('selected_bloco');
	$('#bloco_5').hide();
	$('.sumir_bloco').hide();
	$(this).find('.titulo_bloco_6').addClass('selected_bloco');
	$('.cadastro_some').show();
	});
tinyMCE.execCommand('mceAddControl', true, 'texto'); 	
	  function slideout(){
  setTimeout(function(){
  $("#response").slideUp("slow", function () {
      });
    
}, 2000);}
	
    $("#response").hide();
	$(function() {
	$('#inicio').datepicker({
						showOn: "button",
						buttonImage: "../images/calendar.png",
						buttonImageOnly: false,
						 monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'], 
						 dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
						 dateFormat: 'dd/mm/yy'
						});
						$('#fim').datepicker({
						showOn: "button",
						buttonImage: "../images/calendar.png",
						buttonImageOnly: false,
						 monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'], 
						 dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
						 dateFormat: 'dd/mm/yy'
						});
	$("#items_conul_tab tbody").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
			var order = $(this).sortable("serialize") + '&update=update&pg=<?php echo $_GET['user']; ?>'; 
			$.post("avisos/updateList.php", order, function(theResponse){
				$("#response").html(theResponse);
				$("#response").slideDown('slow');
				slideout();
			}); 															 
		}								  
		});
	});

});	
</script>
<style>
#items_conul_tab{
	cursor:move !important;
} 
#cadastrar_cli .titulo_bloco_6{
		background:#faf6f6;
	-webkit-border-top-left-radius: 25px;
-moz-border-radius-topleft: 25px;
border-top-left-radius: 25px;
}
#cadastrar_cli{
	border:0 none; 
	float: left;
	margin-bottom:0px;
	bottom:-8px;
	cursor:pointer;
	margin-left: 20px; 
}
#cadastrar_cli .titulo_bloco_6:hover, .selected_bloco{
 background:#eee !important;
}

#visualizar_cli .titulo_bloco_6{
		background:#faf6f6;
	-webkit-border-top-left-radius: 25px;
-moz-border-radius-topleft: 25px;
border-top-left-radius: 25px;
}
#visualizar_cli{
	border:0 none; 
	float: left; 
	margin-bottom:0px;
	margin-left: 25px;
	bottom:-8px;
	cursor:pointer;
}
#visualizar_cli .titulo_bloco_6:hover, .selected_bloco{
	background:#eee !important;
}
</style>
</head>
<body>
 <?php $consulta_usuario = mysql_query("SELECT * FROM usuarios WHERE user_id='$_GET[user]'");
	 $exibe_usuario = mysql_fetch_array($consulta_usuario);
	 
	 ?>
<div id="geral">
  <div id="conteudo" style="width:745px; margin-top: -150px;">

	<div class="bloco_6" id="visualizar_cli">
      <div class="titulo_bloco_6 selected_bloco">Visualizar</div>
   </div>
     <?php 
  $consulta_acesso = mysql_query("SELECT * FROM nivel WHERE id='$_SESSION[my_nivel]'");
									$exibe_acesso = mysql_fetch_array($consulta_acesso);
		if($exibe_acesso['detalhes'] == 1 or $_GET['id'] != "" or $_SESSION['my_nivel'] == 0){
  ?>
    <div class="bloco_6" id="cadastrar_cli" >
      <div class="titulo_bloco_6 " >
	 <?php if($_GET['id'] == "" ){
      			echo 'Cadastrar Novo';
				
      		}else if($exibe_acesso['visualizar'] == 1 and $exibe_acesso['detalhes'] == 0 or $_SESSION['my_nivel'] == 0){
				echo 'Exibir ';
			}else{
			echo 'Editar ';
			};?> </div>
   </div>
    <div id="bloco_5" >
	 
      <div class="titulo_bloco_5">
      <?php if($_GET['id'] == ""):
      			echo 'Cadastrar - ';
				
      		else:
				echo 'Alterar - ';
			endif;
			
			echo $tipo_text.'  <small> <span class="vermelho">'.$exibe_usuario['user_name'].' > Aviso > </small>'.$carrega_texto['aviso'].'</span>';
	
		$data_ini = explode('/',$carrega_texto['inicio']);
	$data_fim = explode('/',$carrega_texto['fim']);
	if($carrega_texto['inicio'] != ""){
	$data_inicio = $data_ini[2].'/'.$data_ini[1].'/'.$data_ini[0];
	}
	if($carrega_texto['fim'] != ""){
	$data_fina = $data_fim[2].'/'.$data_fim[1].'/'.$data_fim[0];
	}
	?>	
      </div>
       
        <table border="0" style="margin:0px 20px 10px 20px;">
		 <form method="post" action="avisos/validar.php" id="envia" onsubmit="return false;" />
		 <input type="hidden" id="post_id" name="post_id" value="<?php echo $_GET['id']; ?>" />
		 <input type="hidden" id="usuario" name="usuario" value="<?php echo $_GET['user']; ?>" />
    

			
		
          	  <tr>
            	<td width="100"> Aviso:</td>
             <td>
			 <input type="text" name="aviso" id="aviso" value="<?php echo $carrega_texto['aviso'];?>" size="60" />
               	<span class="seguraduvida"><img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Títutlo do Aviso Cabeçario" /></span></td>
         
			
             </tr> 
           <tr>
            	<td width="100"> Início:</td>
             <td>
			 <input type="text" name="inicio" id="inicio" value="<?php echo $data_inicio; ?>" size="45" />
               	 <span class="seguraduvida"><img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Data para iniciar Exbição" /></span></td>
         
				 </td>
             </tr> 
			 <tr>
            	<td width="100"> Fím:</td>
             <td>
			 <input type="text" name="fim" id="fim" value="<?php echo $data_fina;?>" size="45" />
               	 <span class="seguraduvida"><img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Data para parar exibição" /></span></td>
         
				 </td>
             </tr>
               <tr>
            <td valign="top">Conteúdo:</td>
           <td colspan="3">
		   <textarea  name="texto" id="texto"  rows="15" cols="80" style="width: 80%"><?php echo $carrega_texto['msg'];?></textarea>
           </td>
           <td valign="top"><span class="seguraduvida"><img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Texto sobre o Aviso para o Usuário" /></span></td>
          </tr>
          
          <tr>
          	<td></td>
            <td colspan="3"><input type="submit" name="enviarPost" id="enviarPost" value="Gravar" onclick="tinyMCE.triggerSave(true,true);" /></td>
          </tr>
		  </form>
        </table>
		  </div>

  <?php } ?>
    <div id="bloco_3" class="cadastro_some" style=" margin-top:0px; width: 730px;">
	  <div id="response"> </div>
	
      <div class="titulo_bloco_3" style="width: 700px;">(<?php echo $texto[0];?>) Aviso(s) Cadastrado(s) <span style="float:right;"><?php 	echo ' Avisos  / <span class="vermelho"> '.$exibe_usuario['user_name'].'<small>'; ?></span></div>
      <?php if($texto[0] > 0){?>
      <table width="720" border="0" style="margin-left:10px;">
        <tr class="super">
          <td width="250"><strong>Aviso</strong></td>
          <td width="60"><center><strong>Início</strong></center></td>
          <td width="60"><center><strong>Fim</strong></center></td>
          <td width="70"><center><strong>Finalizar</strong></center></td>
          <td width="25"></td>
          <td width="25"></td>
        </tr></table>

		<table width="720" id="items_conul_tab" border="0" style="margin-left:10px;">
       
       <?php } echo $texto[1];?>
       <?php if($texto[0] > 0){?>
      </table>
      <?php } ?>
    </div>
 
   <div class="creditos" style="margin-left: -330px;">Desenvolvido por <?php echo Criatedbyrodape; ?> - <?php echo date("Y");?> - Todos os direitos reservados &reg;</div>

	  <br clear="all" />
   </div>
<div  class="negro" id="carreg_negro" style="position:fixed;top:50%;left:0; color:#000; width:100%;  z-index:999;"><center class="negro" style="display:none; position:relative; z-index:999;"><img src="../images/carregando.gif" alt="" /></center></div>
<div class="negro" style="background:#000; display:none; position:fixed;   opacity:0.5; left:0; height:100%; width:100%; top:0; z-index:10;" ></div>

</body>
</html>