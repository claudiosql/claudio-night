
//Campo Senha
$(document).ready(function(){

	$("img#SenhaMostrar").click(function(){
			//Reconhecendo o tipo do campo
			var tipoCampoSenha = $("input#cl_senha").attr('type');
			
			//Recolhendo o valor contido no campo
			var valorDoCampo = $("input#cl_senha").val();
			
			//Se o campo for do tipo Senha
			if(tipoCampoSenha == "password"){
			
			 $(".seguraCampo").html("");				
			 $(".seguraCampo").html("<input type='text' name='cl_senha' id='cl_senha' value='"+valorDoCampo+"' />");
				
				
			//Senão for do tipo senha	
			}else{
								
			 $(".seguraCampo").html("<input type='password' name='cl_senha' id='cl_senha' value='"+valorDoCampo+"' />");
			}
		//Movendo o mouse para o campo	
		$("input#cl_senha").focus();
	});
						   
});

//function para Mostrar o campo escondido
	function JanelasMostrarEsconder(ConteudoMostrar){
		
		$("#segura_janelas").toggleClass("none");
		
		if(ConteudoMostrar != ""){
		
			//trabalhando as caracteristicas da janela
			var trab = ConteudoMostrar.split("%");
			
			//Largura
			var jan_largura = trab[0];
			var jan_metade = -trab[0]/2;
			
			//Se a altura da janela for menor ou igual a 400 mantenho a altura disposta pelo usuário caso contrario uso a altura maxima de 400px
			if(trab[1] <= 400){
				
			var jan_altura = trab[1];
			
			}else{
			
			var jan_altura = 400;
			
			}
			//load
			var jan_load = trab[2];
			
			 $("#janela_branca").css({'margin-left' : jan_metade, 'width' : jan_largura, 'height' : jan_altura})
			
			$("#janela_branca").html('<iframe allowtransparency="true" scrolling="no" frameborder="0" width="'+jan_largura+'" height="'+jan_altura+'" src="'+jan_load+'"></iframe>');
			
		}
		
	}


///Gerador de Senhas
	$(document).ready(function(){
	
		$("img#gerador").click(function(){
			
			//var caracteristicas = $(this).attr("lang");
			
			JanelasMostrarEsconder($(this).attr("lang"));
			
		});
		
		$("#janela_preta").click(function(){
			
			JanelasMostrarEsconder();
			
		});
		
								  
	});

//Gravra editar usuário

	$(document).ready(function(){
	
		$("input#Enviar_cl").click(function(){
			
			if($("input#cl_name").val() == ""){

				$("input#cl_name").addClass("error");
				$("input#cl_name").focus();
				

			}else if($("input#cl_mail").val() == ""){

				$("input#cl_mail").addClass("error");
				$("input#cl_mail").focus();

			}else {
				EviarParaCadastro();
			}
			
			
		});
		
	});

//enviar para validação

	function EviarParaCadastro(){

	/*	cl_name = $("input#cl_name").val();
		cl_mail = $("input#cl_mail").val();
		cl_senha = $("input#cl_senha").val();
		cl_nivel = $("select#cl_nivel").val();
		cl_id = $("input#cl_id").val();
*/
		$.post("users/users_valida.php?caso=2",
		{
			cl_id:$("input#cl_id").val(),
			cl_name:$("input#cl_name").val(),
			cl_mail:$("input#cl_mail").val(),
			cl_senha:$("input#cl_senha").val(),
			cl_nivel:$("select#cl_nivel").val()
		}
		,
		function(RespondeCadastroUsers){

			if(RespondeCadastroUsers == 500){
				alert('Gravado com sucesso!');
				window.location="users.php";

			}else if(RespondeCadastroUsers == 600){
				alert('Gravado com sucesso!');
				window.location="users.php?id="+$('input#cl_id').val()+"";
				}else{
				
				alert(RespondeCadastroUsers);
			
			}
		});


	}
	
	
	//Excluir
	function ExcluirUsuario(r){
		
			if(confirm("Tem certeza que deseja apagar o usuário?")){
				
			
					apagueagora(r);
			
			}else{
				
				
			}
		
		}
	
	function apagueagora(r){
		
		$.get("users/users_valida.php?caso=3",
			  {r:r},
			  function(respondeDell){
				  
				if(respondeDell == 500){
				
					window.location="users.php";
					
				}else{
					
					alert(respondeDell);	
					
				}
				
			});
		
	}