﻿	<?php 


//Verificando se o usuário está logado
	require("../reconhece.php");

//funções da classe usuário
	require("banners/function.php");
	//Meus resultados
	$editions = allEditions($_GET['pg']);
	
	

	if($_GET['id'] != ""){
		
		$selecionado = editionLimited($_GET['id']);	
		
	}
	$pagina_sub = Mostrar_sub($_GET['pg']);
	$pagina_sub_pg = Mostrar_pg($_GET['pg']);
	//Aqui vai o numero da pagina
	$pagina_id = 1;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo Criatedby; ?> Administrator</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script type="text/javascript" src="../js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="../js/legendas.js"></script>
<script type="text/javascript" src="../js/jquery.maskedinput-1.2.2.js"></script>
<!-- Funções -->
<script type="text/javascript" src="banners/functions.js"></script>
<!-- Fim Uusarios Funções -->
<link rel="stylesheet" type="text/css" href="../js/modal/shadowbox.css" />
<script src="../js/modal/shadowbox.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/nicEdit.js"></script>
<script type="text/javascript">

Shadowbox.init({
    language: 'pt-BR',
    players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
});

$(function(){
$('#links').click(function(){
$('#linka').show();
$('#texto').hide();
});
$('#textos').click(function(){
$('#linka').hide();
$('#texto').show();

				nicEditors.allTextAreas();
});
 $("select#tipo").change(function () {
          var str = "";
          $("select option:selected").each(function () {
               if($(this).text() == "Banner Topo 920 x 220px"){
			
				
				$('#texto').hide();
			   $('.sele').show();
			   $('#legendas').show();
				
			   }
			   else{
			   $('#legendas').hide();
			   $('.sele').hide();
				$('#texto').hide();
			   }
				
              });
          
		
		  
        })
        .trigger('change');
});
</script>
</head>
<body>

<div id="segura_janelas" class="none">
	<div id="janela_preta"></div>
	<div id="janela_branca"></div>
</div>
<div id="top_geral">
  <div id="top">
    <div class="logo_cms"><img src="../images/logoadm.png" alt="nl2br" height="70" /></div>
    <div class="logo_cli"><img src="../images/logo.png"  alt="logo" title="logo" /></div>
    <?php include("top_menu.php");?>
  </div>
</div>
<div id="geral">
  <div id="conteudo">
    <div id="bloco_1">
      <div class="titulo_bloco_1">
      <?php if($_GET['id'] == ""):
      			echo 'Cadastrar';
      		else:
				echo 'Alterar';
			endif;
				echo ' Imagens  / <span class="vermelho"><small>'.$pagina_sub_pg['titulo'].' > </small>'.$pagina_sub[1].'</span>';

	?>	
      </div>
      <input type="hidden" name="edition_id" id="edition_id" value="<?php echo $selecionado[0]; ?>"/>
      <input type="hidden" name="pg" id="pg" value="<?php echo $_GET['pg']; ?>" />
        <table border="0" style="margin:0px 20px 10px 20px;">
		<tr>
            <td width="120"><strong>Titulo: </strong></td>
            <td>
			<input type="text" name="titulo" id="titulo" name="titulo" value="<?php echo $selecionado[2];?>" />
            </td>
          </tr>
		  
         
          <tr>
            <td><strong><span class="vermelho">*</span>Imagem</strong></td>
            <td>
			<span class="vermelho" style="font-size:9px;">Medida: Largura <?php echo $pagina_sub['width_img']?> pixel X Altura <?php echo $pagina_sub['height_img']?> pixel</span>
             <br />
			<input type="text" name="edition_cover_image" id="edition_cover_image" size="44" value="<?php echo $selecionado[1];?>" />
            	<span class="seguraduvida">
               		<a rel="shadowbox; width=690; height=500;" href="midias/index.php?campo=edition_cover_image">
                    	<img src="../images/bt-seta2.png" width="24" height="24" class="legenda" title="Escolher a imagem" border="0" alt="Escolher-img" /></a>
                </span>
                <span class="seguraduvida">
                	<img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="A imagem que será destacada na index!" />
                </span>
           </td>
          </tr>
		  
		
		
		  	<tr id="linka" >
            <td width="120"><strong> Link: </strong></td>
            <td>
			<input style="width:300px;" type="text" name="link" id="link"  value="<?php echo $selecionado[4];?>" />
            </td>
          </tr>
	
		 
          <tr>
          	<td></td>
            <td colspan="2"><input type="submit" name="edition_gravar" id="edition_gravar" value="Gravar" /></td>
          </tr>
        </table>
    </div>
    <div id="bloco_2">
      <div class="titulo_bloco_2">Guia Rápido</div>
      <ul>
      		<li>Os campos marcados com <span class="vermelho"><strong> * </strong></span> são obrigatórios.</li>
            <li>Em caso de dúvidas posicione o cursor do mouse sobre o ícone <img src="../images/Help1.png" alt="boia" width="15" /> (boia) ao lado do campo.</li>
            <li>Clique no ícone <img src="../images/bt-seta2.png" width="15"  alt="seta"/> (Seta) para abrir o gerenciador de imagens.</li>
           
      </ul>
    </div>
    <div id="bloco_3">
      <div class="titulo_bloco_3">(<?php echo $editions[0];?>) Registro(s) Cadastrado(s) </div>
      <?php if($editions[0] > 0){?>
      <table width="900" border="0" style="margin-left:10px;">
        <tr class="super">
          <td width="100"><strong>Titulo</strong></td>
          <td width="300"><strong>Banner</strong></td>
          <td width="25"></td>
          <td width="25"></td>
        </tr>
       <?php } echo $editions[1]; ?>
       <?php if($editions[0] > 0){ ?>
      </table>
      <?php } ?>
    </div>
  </div>
   <div class="creditos">Desenvolvido por <?php echo Criatedbyrodape; ?> - <?php echo date("Y");?> - Todos os direitos reservados &reg;</div>

	  <br clear="all" />
</div>
 
</body>
</html>
