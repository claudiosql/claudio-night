﻿<?php 

//Verificando se o usuário está logado
	require("../reconhece.php");

//funções da classe usuário
	require("cadastrar_pg/functions.php");
	//Meus resultados
	$posts = listare_pg();	
	$editar = mostrarPg($_GET['id']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo Criatedby; ?> Administrator</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script type="text/javascript" src="../js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="../js/legendas.js"></script>
<script type="text/javascript" src="../js/jquery.maskedinput-1.2.2.js"></script>
<!-- Funções -->
<script type="text/javascript" src="cadastrar_pg/functions.js"></script>
<!-- Fim Uusarios Funções -->
<link rel="stylesheet" type="text/css" href="../js/modal/shadowbox.css" />
<script type="text/javascript" src="../js/tiny_mce.js"></script>
<script src="../js/modal/shadowbox.js" type="text/javascript"></script>
<script type="text/javascript">
Shadowbox.init({
    language: 'pt-BR',
    players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
	
});


</script>
</head>
<body>
<div id="top_geral">
  <div id="top">
    <div class="logo_cms"><img src="../images/logoadm.png" alt="nl2br" height="70" /></div>
    <div class="logo_cli"><img src="../images/logo.png"  alt="logo" title="logo" /></div>
    <?php include("top_menu.php");?>
  </div>
</div>
<div id="geral">
  <div id="conteudo">
    <div id="bloco_5">
      <div class="titulo_bloco_5">
      <?php if($_GET['id'] == ""):
      			echo 'Cadastrar';
      		else:
				echo 'Alterar';
			endif;
			echo ' Página';
	?>	
      </div>
       
        <table border="0" style="margin:0px 20px 10px 20px;">
		 <form method="post" action="cadastrar_pg/validar.php" id="envia_cad" onsubmit="return false">
	      <input type="hidden" name="post_id" id="post_id" value="<?php echo $editar[0]; ?>"/>
          <tr>
            <td width="100"><strong><span class="vermelho">*</span> Nome: </strong></td>
            <td colspan="4"><input type="text" name="nome" id="nome" size="50" value="<?php echo $editar[1];?>" /><span class="seguraduvida"><img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Nome da página" /></span></td>
          </tr>
		   <tr>
            <td width="100"><strong><span class="vermelho">*</span> Posição: </strong></td>
            <td colspan="4"><input type="text" name="posicao" id="posicao" size="5" value="<?php echo $editar[3];?>" /><span class="seguraduvida"><img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Ordem apartir de 0 -" /></span></td>
          </tr>
         
          <tr>
          	<td></td>
            <td colspan="3"><input type="submit" name="enviarPost" id="enviarPost" value="Gravar" /></td>
          </tr>
		  </form>
        </table>
		
    </div>
   
    <div class="bloco_6">
      <div class="titulo_bloco_6">Guia Rápido</div>
      <ul>
      		<li>Os campos marcados com <span class="vermelho"><strong> * </strong></span> são obrigatórios.</li>
            <li>Em caso de dúvidas posicione o cursor do mouse sobre o ícone <img src="../images/Help1.png" alt="boia" width="15" /> (boia) ao lado do campo.</li>
      </ul>
    </div>
    <div id="bloco_3">
	
	  <div id="response"> </div>
      <div class="titulo_bloco_3">(<?php echo $posts[0];?>) Página(s) Cadastrada(s) </div>
      <?php if($posts[0] > 0){?>
      <table width="900" border="0" style="margin-left:10px;">
        <tr class="super">
          <td width="200"><strong>Título</strong></td>
          <td><strong>Tipo:</strong></td>
          <td width="30"><strong>ADD:</strong></td>
          <td width="30"></td>
          <td width="30"></td>
        </tr>
       <?php } echo $posts[1];?>
       <?php if($posts[0] > 0){?>
      </table>
      <?php } ?>
    </div>

   <div class="creditos">Desenvolvido por <?php echo Criatedbyrodape; ?> - <?php echo date("Y");?> - Todos os direitos reservados &reg;</div>
 
	  <br clear="all" />
 </div>
   </div>

</body>
</html>
