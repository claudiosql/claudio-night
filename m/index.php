<?php 
//Verificando se o usuário está logado
	require("../reconhece.php");

$pagina_id = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo Criatedby; ?> Administrator</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script type="text/javascript" src="../js/jquery-1.4.1.js"></script>
<link rel="stylesheet" type="text/css" href="../js/modal/shadowbox.css" />
<script src="../js/modal/shadowbox.js" type="text/javascript"></script>
<script type="text/javascript">
Shadowbox.init({
    language: 'pt-BR',
    players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
	
});
</script>
</head>
<body>
<div id="top_geral">
  <div id="top">
    <div class="logo_cms"><img src="../images/logoadm.png" alt="nl2br" height="70" /></div>
    <div class="logo_cli"><img src="../images/logo.png" alt="logo do cliente" /></div>
    <?php 
	
	include("top_menu.php");
		
	?>
  </div>
</div>
<div id="geral">
  <div id="conteudo"> 
    <div style="font-size:15px; margin:15px;">
    <h3>Olá <small><?php echo $_SESSION['my_name'];?></small></h3><br />
    <p>Seja bem vindo ao <strong><?php echo Criatedby; ?></strong>, por essa área você pode 								administrar o conteúdo do seu site de forma fácil e prática.<br /><br />
      <strong>O <?php echo Criatedby; ?></strong> é um ferramenta que permite o gerenciamento de informações em diversas sessões do site.<br />
      <br />
      <strong>Funcionalidade: </strong> Permite ao administrador gerenciar o								conteúdo publicado no site, como textos, galerias e outros.<br />
      <br />
      <strong>Segurança: </strong>O administrador s&oacute; se conecta	através de um usu&aacute;rio e senha de acesso pr&eacute;viamente cadastrada.<br />
      <br />
      <strong>Armazenamento: </strong>As informações são armazenadas em um banco de dados, permitindo futuras alterações, inserções ou exclusões.<br />
      <br />
      <strong>Vantagens: </strong> Com essa ferramenta as informações no	site estarão sempre atualizadas, de maneira ágil.<br />
<strong>Foi Criada uma conexão com Banco de dados em seu root: code<br /> &#60;?php include "conexao_db.php"; ?> </strong>   
   </p>
    </div>
	
  </div>

<div class="creditos">Desenvolvido por <?php echo Criatedbyrodape; ?> - <?php echo date("Y");?> - Todos os direitos reservados &reg;</div>

	  <br clear="all" />
</div>
</body>
</html>
