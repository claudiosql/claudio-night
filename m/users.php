<?php 





//Verificando se o usuário está logado
	require("../reconhece.php");

//funções da classe usuário
	require("users/functions_users.php");
	//Meus resultados
	$users = ListarUsersCads();
	
	
	if($_GET['id'] != ""){
	
		
		$selecionado = ConsultarEditar($_GET['id']);
	
	}

	$pagina_id = 8;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo Criatedby; ?> Administrator</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script type="text/javascript" src="../js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="../js/legendas.js"></script>
<!-- Uusarios Funções -->
<script type="text/javascript" src="users/functions.js"></script>
<!-- Fim Uusarios Funções -->
<link rel="stylesheet" type="text/css" href="../js/modal/shadowbox.css" />
<script src="../js/modal/shadowbox.js" type="text/javascript"></script>
<script type="text/javascript">
Shadowbox.init({
    language: 'pt-BR',
    players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
	
});
</script>
</head>
<body>
<div id="segura_janelas" class="none">
	<div id="janela_preta"></div>
	<div id="janela_branca">
    	<?php include("users/gerador_senhas.php");?>
    </div>
</div>
<div id="top_geral">
  <div id="top">
    <div class="logo_cms"><img src="../images/logoadm.png" alt="nl2br" height="70" /></div>
    <div class="logo_cli"><img src="../images/logo.png"  alt="logo" title="logo" /></div>
    <?php include("top_menu.php");?>
  </div>
</div>
<div id="geral">
  <div id="conteudo">
    <div id="bloco_1">
      <div class="titulo_bloco_1">Cadastrar novo usuário</div>
      <input type="hidden" name="cl_id" id="cl_id" value="<?php echo $selecionado[0]; ?>"/>
        <table border="0" style="margin:0px 20px 10px 20px;">
          <tr>
            <td width="80"><strong><span class="vermelho">*</span> Nome: </strong></td>
            <td><input type="text" name="cl_name" id="cl_name" size="50" value="<?php echo $selecionado[1];?>" />
            	<span class="seguraduvida">
                	<img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Nome do usuário!" />
                </span>
           </td>
          </tr>
          <tr>
            <td width="80"><strong><span class="vermelho">*</span> E-mail: </strong></td>
            <td><input type="text" name="cl_mail" id="cl_mail" size="50" value="<?php echo $selecionado[2];?>" />
            	<span class="seguraduvida">
                	<img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="E-mail do usuário!" />
                </span>
           </td>
          </tr>
          <tr>
            <td width="80"><strong><span class="vermelho">*</span> Senha: </strong></td>
            <td><span class="seguraCampo"><input type="password" name="cl_senha" id="cl_senha" size="20" /></span>
            <span class="seguraduvida"><img src="../images/lock.png" class="legenda" alt="boia" title="Visualizar Conteúdo" id="SenhaMostrar" /></span>
            <span class="seguraduvida"><img src="../images/reload 32.png" width="23" class="legenda" alt="boia" title="Abrir Gerador de Senhas" id="gerador"  /></span>
            <span class="seguraduvida"><img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Senha do usuário!" /></span></td>
          </tr>
          <?php $consulta_acesso = mysql_query("SELECT * FROM nivel WHERE id='$_SESSION[my_nivel]'");
									$exibe_acesso = mysql_fetch_array($consulta_acesso);
									
		if($exibe_acesso['detalhes'] == 1 or $_SESSION['my_nivel'] == 0){	
									 ?>
          <tr>
            <td width="80"><strong><span class="vermelho">*</span> Nível: </strong></td>
            <td>
            	<select name="cl_nivel" id="cl_nivel">
				<?php if($_SESSION['my_nivel'] == 0){ ?>
                <option value="0">Suporte</option>
				<?php } ?>
				<?php echo Niveis(); ?>
            	</select>
          <span class="seguraduvida"><img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Nível do usuário" /></span></td>
          </tr>
          <?php } ?>
          <tr>
          	<td></td>
            <td colspan="2"><input type="submit" name="Enviar_cl" id="Enviar_cl" value="Gravar"  /></td>
          </tr>
        </table>
    </div>
    <div id="bloco_2">
      <div class="titulo_bloco_2">Guia Rápido</div>
      <ul>
      		<li>Os campos marcados com <span class="vermelho"><strong> * </strong></span> são obrigatórios.</li>
            <li>Em caso de dúvidas posicione o cursor do mouse sobre o ícone <img src="../images/Help1.png" alt="boia" width="15" /> (boia) ao lado do campo.</li>
            <li>O ícone <img src="../images/lock.png" alt="Lock" width="15" /> (cadeado) mostra o valor contino no campo senha.</li>
            <li>Clique no ícone <img src="../images/reload 32.png" alt="boia" width="15" /> (reload) para abrir o gerador de senhas.</li>
            <li>Um e-mail valido deverá conter uma <span class="vermelho"><strong>@</strong></span>.</li>
            <li>Uma senha para ser válida deverá ter ao menos <span class="vermelho"><strong>6</strong></span> caracteres.</li>
      </ul>
    </div>
      <?php 
  
		if($exibe_acesso['detalhes'] == 1 or $_SESSION['my_nivel'] == 0){	
  ?>

    <div id="bloco_3">
      <div class="titulo_bloco_3">(<?php echo $users[0];?>) Usuário(s) Cadastrado(s) </div>
      <table width="900" border="0" style="margin-left:10px;">
        <tr class="super">
          <td><strong>Nome</strong></td>
          <td><strong>E-mail</strong></td>
          <td width="100"><strong>Nível</strong></td>
          <td><strong>Cadastrado em:</strong></td>
          <td width="30">Avisos</td>
          <td width="23"></td>
          <td width="23"></td>
        </tr>
       <?php echo $users[1];?>
      </table>
    </div>
    <?php } ?>
   <div class="creditos">Desenvolvido por <?php echo Criatedbyrodape; ?> - <?php echo date("Y");?> - Todos os direitos reservados &reg;</div>

	  <br clear="all" />
	  
  </div>
   </div>
</body>
</html>
