<?php 

	require("../../reconhece.php");

?>


<link rel="stylesheet" type="text/css" href="js/jquery.plupload.queue/css/jquery.plupload.queue.css" />
<script type="text/javascript" src="js/jquery_google.js"></script>

<!-- Third party script for BrowserPlus runtime (Google Gears included in Gears runtime now) -->
<script type="text/javascript" src="js/browerplus_fin.js"></script>

<!-- Load plupload and all it's runtimes and finally the jQuery queue widget -->
<script type="text/javascript" src="js/plupload.full.js"></script>
<script type="text/javascript" src="js/jquery.plupload.queue/jquery.plupload.queue.js"></script>

<script type="text/javascript">

// Convert divs to queue widgets when the DOM is ready
$(function() {
	$("#uploader").pluploadQueue({
		// General settings
		runtimes : 'html5',
		url : 'upload.php?diretorio=<?php echo $_GET['diretorio']; ?>',
    max_file_size : '10mb',
            chunk_size : '1mb',
            unique_names : false,
            multipart:true,
 resize : {width : 800, quality : 90},
	
            // Redimensiona as imagens do lado do cliente, quando poss�vel
 
            // Especifica os tipos de arquivos v�lidos
            filters : [
                {title : "Todos Arqvuivos", extensions : "jpg,gif,png,jpeg,zip,rar,rmvb,avi,psd,xls,xlsx,doc,docx,mp3,mp4,pdf,"}
            ]
        });
 
        // HTML4
        $("#html4_uploader").pluploadQueue({
            // Configura��es Gerais
            runtimes : 'html4',
			url : 'upload.php?diretorio=<?php echo $_GET['diretorio']; ?>',
        });
    });
</script>

<script type="text/javascript">
	$(function(){
	if($('#num_con').val() != ""){
		$('.usar_conj_hide').hide();
}
});
</script>
<form>
	<div id="uploader">
		<p>Aguarde...</p>
	</div>
</form>
<div id="minhas_image">
<h3 style="font-size:100%; margin:0 0 10px 10px;">Imagen(s) Cadastrada(s) || <a href="javascript:Refresh(<?php echo $_GET['diretorio']; ?>);" ><img src="../../images/refresh.png" /> Atualizar</a></h3>
<ul style="height:385px;width:355px;">
<?php 
	require("functions.php");
	echo listar($_GET['diretorio']);
	
?>
</ul>

</div>