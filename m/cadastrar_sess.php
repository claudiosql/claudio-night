﻿<?php 

//Verificando se o usuário está logado
	require("../reconhece.php");

//funções da classe usuário
	require("cadastrar_sess/functions.php");
	//Meus resultados
	$posts = listare_pg();	
	$editar = mostrarPg($_GET['id']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo Criatedby; ?> Administrator</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script type="text/javascript" src="../js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="../js/legendas.js"></script>
<script type="text/javascript" src="../js/jquery.maskedinput-1.2.2.js"></script>
<!-- Funções -->
<script type="text/javascript" src="cadastrar_sess/functions.js"></script>
<!-- Fim Uusarios Funções -->
<link rel="stylesheet" type="text/css" href="../js/modal/shadowbox.css" />
<script type="text/javascript" src="../js/tiny_mce.js"></script>
<script src="../js/modal/shadowbox.js" type="text/javascript"></script>
<script type="text/javascript">
Shadowbox.init({
    language: 'pt-BR',
    players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
	
});


</script>
</head>
<body>
<div id="top_geral">
  <div id="top">
    <div class="logo_cms"><img src="../images/logoadm.png" alt="nl2br" height="70" /></div>
    <div class="logo_cli"><img src="../images/logo.png"  alt="logo" title="logo" /></div>
    <?php include("top_menu.php");?>
  </div>
</div>
<div id="geral">
  <div id="conteudo">
    <div id="bloco_5">
      <div class="titulo_bloco_5">
      <?php if($_GET['id'] == ""):
      			echo 'Cadastrar';
      		else:
				echo 'Alterar';
			endif;
			echo ' Nível';
	?>	
      </div>
       
        <table border="0" style="margin:0px 20px 10px 20px;">
		 <form method="post" action="cadastrar_sess/validar.php" id="envia_cad" onsubmit="return false">
	      <input type="hidden" name="post_id" id="post_id" value="<?php echo $editar[0]; ?>"/>
          <tr>
            <td width="120"><strong><span class="vermelho">*</span> Nome do Nivel: </strong></td>
            <td colspan="4"><input type="text" name="titulo" id="titulo" size="50" value="<?php echo $editar[1];?>" /><span class="seguraduvida"><img src="../images/Help1.png" width="23" class="legenda" alt="boia" title="Nome da Permissão" /></span></td>
          </tr> 
		  <tr>
            <td width="120"><strong></strong></td>
          
		    <td colspan="4">
			<?php 
			
			if($editar['visualizar'] == 1){
			$visualizar = ' checked ';
			}
			
			if($editar['cadastrar'] == 1){
			$cadastrar = ' checked ';
			}
			
			if($editar['editar'] == 1){
			$edita = ' checked ';
			}
		
			?>
			
	<span style="float:left;  padding: 7px 3px; font-weight:bold;"> Exibir: </span>
            <input type="checkbox" <?php echo $visualizar; ?> style=" float:left; margin-top: 8px;margin-left: 5px;" id="visualizar" name="visualizar" value="1" />
			
	
	<span style="float:left; margin-left:26px; padding: 7px 3px; font-weight:bold;"> Cadastrar: </span>
            <input type="checkbox" <?php echo $cadastrar; ?>  style=" float:left; margin-top: 8px;margin-left: 5px;" id="cadastrar" name="cadastrar" value="1" />
			
    
	<span style="float:left; margin-left:45px; padding: 7px 3px; font-weight:bold;"> Editar: </span>
			<input type="checkbox" <?php echo $edita; ?>  style=" float:left;margin-top: 8px; margin-left: 5px;" id="editar" name="editar" value="1" />
		

		
            	
			</td>
		  </tr>
		   <tr>
            <td width="120"><strong></strong></td>
          
		    <td colspan="4">
			<?php 
			
		
			if($editar['excluir'] == 1){
			$excluir = ' checked ';
			}
			if($editar['chat'] == 1){
			$chat = ' checked ';
			}
			if($editar['detalhes'] == 1){
			$detalhes = ' checked ';
			}
			?>

   <span style="float:left; margin-left:-6px; padding: 7px 3px; font-weight:bold;"> Excluir: </span>
            
				<input type="checkbox" <?php echo $excluir; ?>  style=" float:left; margin-top: 8px;margin-left: 5px;" id="excluir" name="excluir" value="1" />
			
		
			<span style="float:left; margin-left:10px; padding: 7px 3px; font-weight:bold;"> Atendimento: </span>
            
				<input type="checkbox" <?php echo $chat; ?>  style=" float:left; margin-top: 8px;margin-left: 5px;" id="chat" name="chat" value="1" />
			
			<span style="float:left; margin-left:10px; padding: 7px 3px; font-weight:bold;"> Administrar: </span>
            
				<input type="checkbox" <?php echo $detalhes; ?>  style=" float:left; margin-top: 8px;margin-left: 5px;" id="detalhes" name="detalhes" value="1" />
			
		
            	
			</td>
		  </tr>
		   <tr>
            <td width="100"><strong><span class="vermelho">*</span> Liberar acesso: </strong></td>
            <td colspan="4">
			<ul class="secoes">
			<li class="secoes_lado"><p><strong style="color:#f00;">Utilitários</strong></p>
				<br />
				<ul>
				
				<?php if(substr_count($editar[2],"-6") == true){ ?>
				<li><p><input type="checkbox" name="secao_check[]"  checked value="-6"/> Newsletter</p></li>
				
				<?php }else{ ?>
				<li><p><input type="checkbox" name="secao_check[]"  value="-6"/> Newsletter</p></li>
				
				
				<?php } ?>
				</ul>
				</li>
				<li class="secoes_lado"><p><strong style="color:#f00;">Usuáro</strong></p>
				<br />
				<ul>
				<?php if(substr_count($editar[2],"-3") == true){ ?>
				<li><p><input type="checkbox" name="secao_check[]" checked value="-3"/> Gerenciar Usuários</p></li>
				<?php }else{ ?>
								<li><p><input type="checkbox" name="secao_check[]"  value="-3"/> Gerenciar Usuários</p></li>
				
				<?php } ?>
				
				<?php
				if($_SESSION['my_nivel'] == 0){
				if(substr_count($editar[2],"-4") == true){ ?>
				<li><p><input type="checkbox" name="secao_check[]" checked value="-4"/> Gerenciar Páginas</p></li>
				<?php }else{ ?>
				<li><p><input type="checkbox" name="secao_check[]"  value="-4"/> Gerenciar Páginas</p></li>
				
				<?php } } ?> 
				
				<?php if(substr_count($editar[2],"-5") == true){ ?>
				<li><p><input type="checkbox" name="secao_check[]" checked value="-5"/> Gerenciar Níveis</p></li>
				<?php }else{ ?>
				<li><p><input type="checkbox" name="secao_check[]"  value="-5"/> Gerenciar Níveis</p></li>
				
				<?php } ?>
				</ul>
				</li>
			<?php echo  Mostrar_sec($editar[2]); ?>
			
			</ul>
			</td>
          </tr>
         
          <tr>
          	<td></td>
            <td colspan="3"><input type="submit" name="enviarPost" id="enviarPost" value="Gravar" /></td>
          </tr>
		  </form>
        </table>
		
    </div>
   
    <div class="bloco_6">
      <div class="titulo_bloco_6">Guia Rápido</div>
      <ul>
      		<li>Os campos marcados com <span class="vermelho"><strong> * </strong></span> são obrigatórios.</li>
            <li>Em caso de dúvidas posicione o cursor do mouse sobre o ícone <img src="../images/Help1.png" alt="boia" width="15" /> (boia) ao lado do campo.</li>
      </ul>
    </div>
    <div id="bloco_3">
      <div class="titulo_bloco_3">(<?php echo $posts[0];?>) Nivel Cadastrado(s) </div>
      <?php if($posts[0] > 0){?>
      <table width="900" border="0" style="margin-left:10px;">
        <tr class="super">
          <td width="450"><strong>Nível</strong></td>
          <td><strong>Acesso:</strong></td>
          <td width="30"></td>
          <td width="30"></td>
        </tr>
       <?php } echo $posts[1];?>
       <?php if($posts[0] > 0){?>
      </table>
      <?php } ?>
    </div>

   <div class="creditos">Desenvolvido por <?php echo Criatedbyrodape; ?> - <?php echo date("Y");?> - Todos os direitos reservados &reg;</div>
 

	  <br clear="all" /> </div>
   </div>

</body>
</html>
