<?php 
if(file_exists("adm/admin-config.php")){
	require("destruir.php");
	require("adm/admin-config.php");
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo Criatedby; ?> Administrator - Faça o login</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>    
<script type="text/javascript">
	//Logar / Recuperar
	function painel(){
			$("#agora").toggleClass("none");	
			$("#recuperar").toggleClass("none");				
	}
	
	//Logar	
	$(document).ready(function(){
		
		$("input#logJa").click(function (){
				
		  $("input").removeClass("error");
								
		   if($("input#user").val() == ""){
				  				
		   		$("input#user").addClass("error");
				$("input#user").focus();
					
		   }else if($("input#pass").val() == ""){
				  
				$("input#pass").addClass("error");
				$("input#pass").focus();
					
		  }else{
		
				myLogAgura();
				
		 }
				
		});	
		$("#logar").submit(function (){
				
		  $("input").removeClass("error");
								
		   if($("input#user").val() == ""){
				  				
		   		$("input#user").addClass("error");
				$("input#user").focus();
					
		   }else if($("input#pass").val() == ""){
				  
				$("input#pass").addClass("error");
				$("input#pass").focus();
					
		  }else{
		
				myLogAgura();
				
		 }
				
		});
		
	});
	
	//MyLogAgura
	
	function myLogAgura(){
			$("#respondeAjax2").html("Aguarde, Carregando...");			
		$.post("logar.php",
		{user: $("input#user").val(), pass: $("input#pass").val()},
		function(logou){
			
			if(logou == 750){
				//Admin
				window.location = "<?php echo Config_Cliente_Nome ;?>/m/index.php";	
				
			}else if(logou == 500){			
				//usuario
				window.location = "<?php echo Config_Cliente_Nome; ?>/m/index.php";	
				
			}else if(logou == 250){
				//funcionário 
				window.location = "<?php echo Config_Cliente_Nome;?>/m/index.php";
			
			}else if(logou == 300){
				//funcionário 
				alert('Navegador incompatível com o sistema!');
			
			}else{
				
				$("#my_div").html(logou);
				
				$("#respondeAjax2").html(logou);
				
			}
		
		});
		
	}
	
	$(document).ready(function(){
		
		$("input#trazer").click(function(){
			
			$("input").removeClass("error");				
			
			if($("input#mail").val() == ""){
				
				$("input#mail").addClass("error");
				$("input#mail").focus();
			
			}else{
			
				$("#respondeAjax").removeClass("none");
				$("#respondeAjax").html("Aguarde, Carregando...");						
				enviarLogin();
			}
			
		});
	});
	
	//EnviarLogin
	
	function enviarLogin(){
	
		mail = $("input#mail").val();
		
		sis = "100";
		
		$.get("destruir.php",
		
		{sis:sis, mail:mail},
		
			function(retornar){
				
				if(retornar == 500){
				
					$("#respondeAjax").html("Por favor, verifique sua caixa de e-mail");
					
				}else{
					
					$("input#mail").addClass("error");
					$("input#mail").focus();
					$("#respondeAjax").html(retornar);
					
				}	
				
			}
			
		);
		
		
	}
		
</script>
<style>
a{
text-decoration:none;
border: 0 none;
}
a img{
border: 0 none;
}</style>
</head>
<body>
<div id="geral">
  <form id="logar" onsubmit="return false;" action=""> <center><img src="images/logoadm.png" width="200" alt="nl2br"/></center>
    <div align="center" id="agora" >
     
        <table border="0" style="margin:15px;">
          <tr>
            <td><label><span class="vermelho">*</span> <b>Usuário:</b></label>
              <input type="email" name="user"  id="user" size="38" maxlength="50" /></td>
          </tr>
          <tr>
            <td><label><span class="vermelho">*</span> <b>Senha:</b></label>
              <input type="password" name="pass" id="pass"  size="38" maxlength="50" /></td>
          </tr>
          <tr>
            <td id="respondeAjax2"></td>
          </tr>
          <tr>
            <td><input type="submit" value="Entrar" id="logJa"  />
              <span class="recuperar"><a href="javascript:painel();">Recuperar Senha</a></span></td>
          </tr>
        </table>
     
    </div>
    <div align="center" id="recuperar" class="none">
      
        <table border="0" style="margin:15px;">
          <tr>
            <td><label><span class="vermelho">*</span> <b>Digite seu e-mail para recuperar a senha:</b></label>
              <input type="text" name="mail" id="mail" size="38" maxlength="50" /></td>
          </tr>
          <tr>
          	<td id="respondeAjax" class="none"></td>
          </tr>
          <tr>
            <td>
              <input type="submit" value="Enviar"  id="trazer" />
              <span class="recuperar"><a href="javascript:painel();">Logar Agora</a></span></td>
          </tr>
        </table>
      
    </div>
<div class="box_fim">
		<span style="float: left;">O sistema <strong><?php echo Criatedby; ?></strong> é compatível apenas<br/>
		com os navegadores: <b>Firefox</b>, <b>Safari</b> e <b>Google<br/>
		Chrome</b>. Para instalar clique no í­cone abaixo:<br/>
		<br/>
		<strong>Baixar:</strong> <span style="float: right; margin-right: 90px; margin-left: 45px; margin-top: -20px;">
					<a href="http://www.getfirefox.com" target="_blank">
												<img src="images/icone_firefox.png" style="margin-right: 10px;" />
											</a>
					<a href="http://www.apple.com/safari/download" target="_blank">
												<img src="images/icone_safari.png" style="margin-right: 10px;" />
											</a>
					<a href="http://www.google.com/chrome" target="_blank">
												<img src="images/icone_chrome.png" />
											</a>
				</span>
		</span> 
		<div style="clear: both;padding-top: 15px;">
			&nbsp;	
		</div>
	</div>
  </form>
</div>
</body>
</html>
